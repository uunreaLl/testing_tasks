def func(search, first, second):
    if search >= max(first, second) or search <= min(first, second):
        return True
    else:
        return False


def task(x1, y1, x2, y2, x3, y3, x4, y4):
    if x1 <= x3 <= x2:
        if min(y1, y2) <= y3 <= max(y1, y2):
            return func(y4, y1, y2)
        elif min(y1, y2) <= y4 <= max(y1, y2):
            return True
        else:
            return False
    elif x3 <= x2 <= x4:
        if min(y3, y4) <= y2 <= max(y3, y4):
            return func(y1, y3, x4)
        elif y3 <= max(y1, y2):
            return True
        else:
            return False
    elif min(x3, x4) <= x1 <= max(x3, x4):
        return True
    else:
        return False


print('OUT:', task(-1, 100, 1, -100, -1, 100, 1, 200))
